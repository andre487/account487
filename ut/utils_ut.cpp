#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE AppUtils

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <cstdlib>

#include "../src/utils.h"

BOOST_AUTO_TEST_SUITE(AppUtils_GetHttpHost) // NOLINT

    BOOST_AUTO_TEST_CASE(GetDefault) { // NOLINT
        unsetenv("HTTP_HOST");

        BOOST_CHECK_EQUAL(AppUtils::get_http_host(), AppUtils::DEFAULT_HTTP_HOST);
    }

    BOOST_AUTO_TEST_CASE(GetFromEnv) { // NOLINT
        setenv("HTTP_HOST", "localhost", 1);

        BOOST_CHECK_EQUAL(AppUtils::get_http_host(), "localhost");
    }

BOOST_AUTO_TEST_SUITE_END() // NOLINT

BOOST_AUTO_TEST_SUITE(AppUtils_GetHttpPort) // NOLINT

    BOOST_AUTO_TEST_CASE(GetDefault) { // NOLINT
        unsetenv("HTTP_PORT");

        BOOST_CHECK_EQUAL(AppUtils::get_http_port(), AppUtils::DEFAULT_HTTP_PORT);
    }

    BOOST_AUTO_TEST_CASE(GetFromEnv) { // NOLINT
        setenv("HTTP_PORT", "8081", 1);

        BOOST_CHECK_EQUAL(AppUtils::get_http_port(), 8081);
    }

    BOOST_AUTO_TEST_CASE(HandleInvalid) { // NOLINT
        setenv("HTTP_PORT", "invalid80_81", 1);

        BOOST_CHECK_THROW(AppUtils::get_http_port(), std::runtime_error);
    }

BOOST_AUTO_TEST_SUITE_END() // NOLINT
