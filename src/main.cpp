#include <iostream>
#include <server_http.hpp>
#include <thread>
#include "log.h"
#include "routes.h"
#include "utils.h"

namespace {
    using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

    std::thread* serve_requests() {
        auto server = new HttpServer{};

        const auto host = AppUtils::get_http_host();
        auto port = AppUtils::get_http_port();

        server->config.address = host;
        server->config.port = port;

        server->on_error = [](std::shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code& /*ec*/) {
            // TODO: enable logging and research "Server error 2: End of file"
            // AppLog::error("Server error " + std::to_string(ec.value()) + ": " + ec.message());
        };

        for (const auto& route : AppRoutes::getRoutes()) {
            const auto& pattern = route.first;

            for (const auto& handler_data : route.second) {
                const auto& http_method = handler_data.first;
                const auto& handler = handler_data.second;

                server->resource[pattern][http_method] = handler;
            }
        }

        auto server_thread = new std::thread([server, host, port]() {
            AppLog::info("Server started to serving requests on host " + host + " and port " + std::to_string(port));
            server->start();
        });

        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        return server_thread;
    }
}

int main() {
    auto server_thread = serve_requests();
    auto logs_thread = AppLog::start_write_logs();
    auto cleanup_thread = AppLog::start_cleanup_time_marks();

    server_thread->join();
    logs_thread->join();
    cleanup_thread->join();

    return 0;
}
