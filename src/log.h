#pragma once

#include <string>
#include <thread>

namespace AppLog {
    const int TIME_MARK_TTL = 3;  // hours

    void info(const std::string& msg) noexcept;

    void error(const std::string& msg) noexcept;

    void time(const std::string& id) noexcept;

    void timeEnd(const std::string& id) noexcept;

    std::thread* start_cleanup_time_marks() noexcept;

    std::thread* start_write_logs() noexcept;
}
