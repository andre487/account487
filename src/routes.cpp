#include "routes.h"
#include <server_http.hpp>
#include <sstream>
#include "log.h"
#include "data.h"

#define DECL_ROUTE(Pattern, Method, Handler)                                                                      \
    {                                                                                                             \
        const auto str_pattern = std::string(#Pattern);                                                           \
        const auto route_time_id = "route " + str_pattern;                                                        \
        const auto thread_time_id = "create route " + str_pattern + " thread";                                    \
                                                                                                                  \
        routeMap[Pattern][Method] = [route_time_id, thread_time_id](HttpResponse response, HttpRequest request) { \
            AppLog::time(thread_time_id);                                                                         \
                                                                                                                  \
            std::thread work_thread([route_time_id, response, request] {                                          \
                AppLog::time(route_time_id);                                                                      \
                Handler(response, request);                                                                       \
                AppLog::timeEnd(route_time_id);                                                                   \
            });                                                                                                   \
            work_thread.detach();                                                                                 \
                                                                                                                  \
            AppLog::timeEnd(thread_time_id);                                                                      \
        };                                                                                                        \
    }

namespace AppRoutes {
    const Routes getRoutes() noexcept {
        Routes routeMap{};

        DECL_ROUTE("^/ping$", "GET",
                   [](HttpResponse response, HttpRequest request) { *response << createResponse("Pong"); });

        DECL_ROUTE("^/ping/slow$", "GET", [](HttpResponse response, HttpRequest request) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            *response << createResponse("Slow pong");
        });

        DECL_ROUTE("^/transactions/latest$", "GET", [](HttpResponse response, HttpRequest request) {
            auto transactions = AppData::Transaction::getLatest();

            *response << transactions.size();
        });

        return routeMap;
    }

    const std::string createResponse(const std::string& content) noexcept {
        std::stringstream resp;

        resp << "HTTP/1.1 200 OK\r\n"
             << "Content-Length: " << content.length() + 2 << "\r\n\r\n"
             << content << "\r\n";

        return resp.str();
    }
}
