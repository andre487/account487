#pragma once

#include <string>
#include <vector>

namespace AppData {
    struct Transaction {
        std::string datetime{};
        std::string currency{};
        std::pair<unsigned int, unsigned int> amount{};
        std::string comment{};
        std::string category{};

        static const std::vector<Transaction> getLatest();
    };

    using Transactions = std::vector<Transaction>;
}
