#include "utils.h"
#include "log.h"

namespace AppUtils {
    const std::string get_http_host() noexcept(false) {
        auto env_host = std::getenv("HTTP_HOST");

        if (env_host == nullptr) {
            return DEFAULT_HTTP_HOST;
        }

        return std::string(env_host);
    }

    unsigned short get_http_port() noexcept(false) {
        auto env_port = std::getenv("HTTP_PORT");

        if (env_port == nullptr) {
            return DEFAULT_HTTP_PORT;
        }

        char** end_ptr = nullptr;
        auto port_num = static_cast<unsigned short>(strtol(env_port, end_ptr, 10));

        if (port_num == 0) {
            throw std::runtime_error("Invalid port in environment: " + std::string(env_port));
        }

        return port_num;
    }
}
