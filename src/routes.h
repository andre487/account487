#pragma once

#include <map>
#include <server_http.hpp>
#include <string>
#include "log.h"

namespace AppRoutes {
    using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

    using HttpRequest = std::shared_ptr<HttpServer::Request>;
    using HttpResponse = std::shared_ptr<HttpServer::Response>;

    using RouteHandler =
        std::function<void(std::shared_ptr<HttpServer::Response>, std::shared_ptr<HttpServer::Request>)>;

    using Routes = std::map<std::string, std::map<std::string, RouteHandler>>;

    const Routes getRoutes() noexcept;

    const std::string createResponse(const std::string& content) noexcept;
}
