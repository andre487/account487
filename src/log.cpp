#define BOOST_SPIRIT_THREADSAFE

#include "log.h"
#include <boost/lockfree/queue.hpp>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <map>
#include <mutex>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>

namespace {
    using std::chrono::steady_clock;
    using std::chrono::system_clock;

    struct LogLineData {
        std::string datetime{};
        std::string level{};
        std::string msg{};

        LogLineData(std::string datetime, std::string level, std::string msg)
            : datetime{std::move(datetime)}, level{std::move(level)}, msg{std::move(msg)} {
        }
    };

    boost::lockfree::queue<LogLineData*> log_queue(128);  // NOLINT

    std::mutex log_mtx{};
    std::condition_variable log_signal{};

    std::map<std::string, steady_clock::time_point> time_marks{};

    std::string get_log_datetime() noexcept {
        const auto now = system_clock::now();
        const auto in_time_t = system_clock::to_time_t(now);

        std::stringstream res{};
        res << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %H:%M:%S");
        return res.str();
    }

    void queue_line(const std::string& level, const std::string& msg) noexcept {
        log_queue.push(new LogLineData(get_log_datetime(), level, msg));
        log_signal.notify_all();
    }

    const std::string dress_time_mark_id(const std::string& mark_id) {
        const auto& thread_id = std::this_thread::get_id();

        std::stringstream dressed_id{};

        dressed_id << thread_id << " :: " << mark_id;

        return dressed_id.str();
    }

    void cleanup_time_marks() noexcept {
        int erased = 0;

        for (const auto& item : time_marks) {
            const auto now_point = steady_clock::now();
            auto diff = std::chrono::duration_cast<std::chrono::hours>(now_point - item.second);

            if (diff.count() > AppLog::TIME_MARK_TTL) {
                time_marks.erase(item.first);
                erased++;
            }
        }

        AppLog::info("Time marks cleanup finished. " + std::to_string(erased) + " items were erased");
    }

    void write_lines() noexcept {
        std::unique_lock<std::mutex> lck(log_mtx);
        LogLineData* data{};

        while (true) {
            while (log_queue.pop(data)) {
                std::clog << data->datetime << '\t' << data->level << '\t' << data->msg << std::endl;
                delete data;
            }

            log_signal.wait(lck);
        }
    }
}

namespace AppLog {
    void info(const std::string& msg) noexcept {
        queue_line("INFO", msg);
    }

    void error(const std::string& msg) noexcept {
        queue_line("ERROR", msg);
    }

    void time(const std::string& id) noexcept {
        const auto dressed_id = dress_time_mark_id(id);
        time_marks[dressed_id] = steady_clock::now();
    }

    void timeEnd(const std::string& id) noexcept {
        const auto dressed_id = dress_time_mark_id(id);

        if (time_marks.count(dressed_id) == 0) {
            return;
        }

        const auto start_time_point = time_marks[dressed_id];
        const auto now_point = steady_clock::now();
        const auto diff = now_point - start_time_point;

        const auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(diff);
        time_marks.erase(dressed_id);

        std::stringstream msg{};

        msg << "Time for " << id << " = " << duration.count() / 1e6 << " ms";

        queue_line("PERF", msg.str());
    }

    std::thread* start_cleanup_time_marks() noexcept {
        auto cleanup_thread = new std::thread([]() {
            while (true) {
                cleanup_time_marks();
                std::this_thread::sleep_for(std::chrono::hours(AppLog::TIME_MARK_TTL));
            }
        });

        return cleanup_thread;
    }

    std::thread* start_write_logs() noexcept {
        return new std::thread(write_lines);
    }
}
