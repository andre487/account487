#pragma once

#include <cstdlib>
#include <string>

namespace AppUtils {
    const std::string DEFAULT_HTTP_HOST = "127.0.0.1";  // NOLINT
    const unsigned short DEFAULT_HTTP_PORT = 8080;

    const std::string get_http_host() noexcept(false);

    unsigned short get_http_port() noexcept(false);
}
